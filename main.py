import json
import plotly
import plotly.graph_objs as go

name = 'vdmakagonova@miem.hse.ru'



jitsi_grade = 0
j_session = dict()
with open('/home/student/rawData/JitsiSession.json', 'r', encoding='utf-8') as f:
    for line in f:
        data = json.loads(line)
        for i in data:
            if i['username'] == name:
                j_session[i['date']] = j_session.get(i['date'], 0) + 1

j_visits_x = [i for i,j in j_session.items()]
j_visits_y = [1 + i for i in range(len(j_session))]

if len(j_session) > 1:
    jitsi_grade = 0.5 * j_visits_y[-1]





git_grade = 0
gtv = dict()
with open('/home/student/rawData/GitStats.json', 'r', encoding='utf-8') as f:
    for line in f:
        data = json.loads(line)
        #print(json.dumps(data, indent=2, ensure_ascii=False))
        for i in data:
            if i['email'] == name:
                git_grade = 1
                for k in i['projects']:
                    if k['name'] == "БИВ20-Микропроект / "+name.split("@")[0]:
                        for j in k["commits"]:
                            gtv[j['committed_date']] = gtv.get(j['committed_date'], 0) + 1
gtv_x = [i for i,j in gtv.items()]
gtv_y = [j for i,j in gtv.items()]
for i in range(len(gtv_y)-1):
    gtv_y[i+1] = gtv_y[i] + gtv_y[i+1]

if len(gtv) > 1:
    git_grade += 1




zulip_grade = 0
zpv = dict()
with open('/home/student/rawData/ZulipStats.json', 'r', encoding='utf-8') as f:
    for line in f:
        data = json.loads(line)
        for i in data:
            if i['email'] == name:
                zulip_grade = 1
                for j in i['messages']:
                    zpv[j['timestamp'][:10]] = zpv.get(j['timestamp'][:10], 0) + 1
zpv_x = [i for i,j in zpv.items()]
zpv_y = [j for i,j in zpv.items()]
for i in range(len(zpv_y)-1):
    zpv_y[i+1] = zpv_y[i] + zpv_y[i+1]

if len(zpv) > 1:
    zulip_grade += 1




postern = 0
pst = dict()
with open('/home/student/rawData/JitsiSession.json', 'r', encoding='utf-8') as f:
    for line in f:
        data = json.loads(line)
        for i in data:
            if 'project' in i['room'] and i['username'] == name:
                    pst[i['date']] = pst.get(i['date'], 0) + 1
pst_x = [i for i,j in pst.items()]
pst_y = [1 + i for i in range(len(pst))]

if len(pst) > 1:
    postern = 0.5 * postern_y[-1]





grade = jitsi_grade + git_grade + zulip_grade+postern





fig = go.Figure()
fig.add_trace(go.Scatter(x=j_visits_x,
                         y=j_visits_y,
                         mode="lines+markers",
                         line=dict(width=2, color='Red'),
                         marker=dict(size=5),
                         name='Посещения Jitsi'))

fig.add_trace(go.Scatter(x=gtv_x,
                         y=gtv_y,
                         mode="lines+markers",
                         line=dict(width=2, color='Blue'),
                         marker=dict(size=5),
                         name='Коммиты в GitLab'))

fig.add_trace(go.Scatter(x=zpv_x,
                         y=zpv_y,
                         mode="lines+markers",
                         line=dict(width=2, color='Green'),
                         marker=dict(size=5),
                         name='Сообщения в Zulip'))

fig.add_trace(go.Scatter(x=pst_x,
                         y=pst_y,
                         mode="lines+markers",
                         line=dict(width=2, color='Orange'),
                         marker=dict(size=5),
                         name=' Посещение постерной сессии'))

fig.update_xaxes(title_text='Итоговая оценка: '+str(grade),
                 title_font = {"size": 30})                         

fig.update_layout(legend=dict(orientation="h",
    			      yanchor="bottom",
			      y=1.02,
			      xanchor="right",
			      x=1))

fig.write_html('/home/student/student_stats/vdmakagonova/vdmakagonova.html')
